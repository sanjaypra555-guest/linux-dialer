#!/usr/bin/env python3

#    setup.py
#    This file is part of Linux Dialer.
#
#    Copyright (C) 2018 Sanjay Prajapat <sanjaypra555@gmail.com>
#
#    Linux Dialer is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Linux Dialer is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Linux Dialer.  If not, see <https://www.gnu.org/licenses/>.


import sys
from os import path
from pathlib import Path
from subprocess import check_call

from setuptools import setup, find_packages
from setuptools.command.install import install

from linuxdialer import __version__ as VERSION

desktop_file_folder = Path('~/.local/share/').expanduser().__str__()
update_mime = 'update-desktop-database ' + desktop_file_folder
print(sys.prefix)
here = path.abspath(path.dirname(__file__))

with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

class PostInstallCommand(install):
    """Post-installation for installation mode."""

    def run(self):
        check_call(update_mime.split())
        install.run(self)


setup(
    name='linuxdialer',
    version=VERSION,
    description='Applications for handling tel: uri and vCard files',
    long_description=long_description,
    url='https://salsa.debian.org/sanjaypra555-guest/linux-dialer/',
    author='Sanjay Prajapat',
    author_email='sanjaypra555@gmail.com',
    keywords='linuxdialer',
    packages=find_packages(exclude=['contrib', 'docs', 'tests', 'test']),
    install_requires=['phonenumbers', 'pytz', 'vobject'],
    scripts=['scripts/linuxdialer'],
    data_files=[('share/applications', ['linuxdialer/resources/linuxdialer.desktop']),
                ('share/icons/hicolor/128x128/apps', ['linuxdialer/resources/linuxdialer.png'])],
    include_package_data=True,
    project_urls={
        "Source": "https://salsa.debian.org/sanjaypra555-guest/linux-dialer/",
    },
    cmdclass={
        'install': PostInstallCommand,
    },
    classifiers=(
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Operating System :: POSIX :: Linux",

        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
    )
)
