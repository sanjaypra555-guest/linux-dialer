---------------------------------------------------------------------------------
            						                ===========================
            							                    Linux Dialer
            						                ===========================
---------------------------------------------------------------------------------
Week 1
=========
- [x] Requirement gathering and project planning

Week 2
=========

2018-05-21
----------
- [x] Design a basic popup Interface

2018-05-22
----------
- [x] Support tel: input

2018-05-23
----------
 - [x] Phone number validation

2018-05-24
----------
 - [x] Display the phone number in national format
 - [x] Provide error message if phone number is invalid

2018-05-25
-----------
 - [x] Provide some additional info about tel: number

 Week 3
======
2018-05-28
-------
- [x] Read about Gnome Contacts and understand the database tables

2018-05-29
-------
- [x] Write SQL query to add phone number to address book.

2018-05-30
------
- [x] Hardcode query into program

2018-05-31
-----
- [x] Design GUI for input (Contact name, email, Address etc. )

2018-06-01
------
- [x] Complete the GUI design


Week 4
======
2018-06-04
-------
- [x] Read about Gnome Contacts and understand the database tables

2018-06-05
-------
- [x] Write SQL query to fetch email from contact database

2018-06-06
------
- [x] Hardcode query into program

2018-06-07
-----
- [x] Make proper UI for displaying email

2018-06-08
------
- [x] Make error message pop-up of email not found



Week 5
======
2018-06-11
-------
- [x] Find ISO code for tel: number using country code ( Will be use in truecaller)

2018-06-12
-------
- [x] Make a data table of country tel: code and country ISO code.

2018-06-13
------
- [ ] Create a HTTP request for truecaller connection, check with proxy network

2018-06-14
-----
- [x] Start working on "Search on Truecaller"

2018-06-15
------
- [x] Continue work "Search on Truecaller" and complete it.



Week 6
======
Note that these do not need to be done in order.

- [x] Implement the interface described in "Button framework" in README.md
  - [x] Copy the existing Qt popup window code to a new module.
  - [x] Write something that draws buttons and handles clicks based on the
    sequence provided to "Application" in the example. Do not support icons yet.
  - [x] Test with the example "print" buttons.
  - [x] Write unit tests to ensure that the correct interface is created with
    the example "print" buttons.
  - [x] Create icons. They don't have to be PNG; they can be in whatever format
    you want. Document the format, dimensions, &c. that is required.
  - [x] Access the fake icons with absolute paths, and incorporate these in the
    program.
  - [x] Write unit tests to ensure that the correct interface is created with
    the icons added.
  - [ ] Tom will tell me how to reference the icons in Python; incorporate these
    changes.
  - [x] Modify the print buttons to return the string instead of printing it.
  - [x] Modify the application to display the returned string.
  - [x] Write tests for this as well.
- [x] Convert the present truecaller module to a launch function within this
  framework.
  - [x] Convert the arguments to match the interface described above.
  - [x] Add it to the sequence of buttons passed to "Application".
    Confirm that it works, but don't write any tests specific to it.
  - [x] Separate the current function into one function that constructs the URL
    (maybe `search_url`) and another function that just calls
    `webbrowser.open(search_url(phonenumber))`.
  - [x] Write tests for `search_url`, using a few different phone numbers.
- [x] Refactor the current `database_utils.py` files into one class.
  - [x] Use the current `initialize` as `__init__`.
  - [x] Use the other methods.
  - [x] Read about sqlite3 transactions.
  - [x] Write unit tests for each method.
  - [x] Additionally, write a test to confirm that the following situation
    raises a transaction error or produces the correct database state:
    1. Create one instance of the class (instance1).
    2. Create another instance of the class (instance2).
    3. Call `instance1.generate_new_uid`
    4. Call `instance2.generate_new_uid`
    5. Call `instance1.write_into_database`
    6. Call `instance2.write_into_database` with different arguments.
    7. The inserted vcards have different uids.
  - [x] Use sqlite3's escape feature instead of string formatting. See "# Never
    do this -- insecure!" and "# Do this instead" in
    https://docs.python.org/3.7/library/sqlite3.html

Week 7
=======

- [x] Improved Button Framework Structure
- [x] Used Relative import instead of sys.path
- [x] Removed phonenumbers_utils.py module and changed the main_window module
- [x] Added Qt test for Add to addressbook
- [x] Added Qt test for Translate to email
- [x] Added Qt test for button framework using mock button data
- [x] Modified program to support command line options
- [x] Added vcard_file support through commandline

Week 8
======
- [x] Created Mime handler script that configure mime type for tel: url and vcard files
    - User can invoke linuxdialer pop-up window by clicking a tel: link in Firefox, Chrome, Evolution etc.
    - User can open vCard file by right clicking and selecting "Open with Linux Dialer" from File Browser
- [x] Handled Default Country
    - Added GUI element into popup window to choose country
    - Created config file in '~/.config/linuxdialer/' directory to store settings
    
Week 9
======
- [x] Implemented graphical interface for vCard extractor
    - Validating vcard file, display error message if file is not valid
    - Display message if vCard doesn't contain any tel number
    - List all the tel numbers with type parameter
    - Added buttons to open corresponding phone number with main popup
- [x] Updated command line arguments, so one .desktop file can be used for both tel and vcard mimes
- [x] Python packaging of project
- [x] Uploaded package to PyPI, Install using pip3 `pip3 install linuxdialer`

Week 10
=====
- [x] Updated python package
    - [x] Added icon in $prefix/share/icons/hicolor/
    - [x] Added launch script and updated setup.py
    - [x] Updated on PyPI.
    
- [x] Debian packaging of project
    - [x] Created debian folder in root directory
    - [x] built .deb package
    - [x] Added in Release

Week 11
======
- [x] Fixed some minor bugs
- [x] Fixed dependencies problem in debian packaging
- [x] Released new .deb package
- [x] Released new python package
- [x] Added documentation for Button Creation Framework (How to add a new button)

Week 12
======
- [x] Added VoIP calling functionality
- [x] Added button in main pop-up using Button Creation Framework
- [x] Modified Button Creation Framework (It can check whether a program which is used in button function is installed or not)
- [x] Released new python and debian package

Task Undetermined
=======
- Call with Signal :-

signal-desktop =  Desktop version doesn't support calling , so it is not feasible to call through signal desktop app.

signal-mobile   = Not feasible


- Call with Skype :-

Can be done easily for old version of skype as it support calling through command line option but new version of skype doesn't. ( Not feasible for new version )

Old Version of skype : skype that comes with **skype** package name

New Version of skype : skype that comes with **skypeforlinux** package name
