# Button Creation Framework Documentation
###### This documentation shows how to add a new button in the main pop-up window.

If you have a single module then name it same as the button.

If you have multiple modules for button function then
 
- create a new folder (give it name same as the button)
- put those modules in the folder
- must contain `__init__.py` module
- use relative imports instead of absolute

===================================

- Put this single_module/folder in `linuxdialer/buttons` directory.
- Put button icon in `linuxdialer/buttons/icons` directory
- single_module ( or `__init__.py` in case of folder) must contain -
    
    - global variable `name = 'Name of button'`
    - global variable `icon = Path(__file__).parent / 'icons' / 'icon_name.png'`, where `icon_name.png` is the file you put in step described above
    - If button function needed some other programs to operate then add those program's name in a global tuple named as `apps_required`. Example - `apps_required = ('ekiga', 'linphone')`.
    If these programs are not installed in the system then button framework will disable the button and notify user to install.
    - method `launch(parent, phonenumber_obj)` where `parent` is the reference of main pop-up window (for GUI actions) and 
`phonenumber_obj` is the phone number object of [phonenumbers](https://github.com/daviddrysdale/python-phonenumbers) library. 
This method should contain code for action to be performed when the button is pressed.

- Open `linuxdialer/buttons/__init__.py` file and add your single_module_name/folder_name in `buttons` tuple by importing it.
Position of the button would be assigned according to index in `buttons` tuple.

- Run the Application :)