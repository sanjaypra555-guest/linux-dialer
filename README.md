# linux-dialer
##### Applications for handling tel: uri and vCard files

![linuxdialer_popup](/screenshots/linuxdialer_popup.png)

## Installation 
##### Using debian package ( Install `phonenumbers` library separately using pip3 )

Download latest debian package from [release](https://salsa.debian.org/sanjaypra555-guest/linux-dialer/tags)

```sh
sudo dpkg -i linuxdialer*.deb
pip3 install phonenumbers
```

##### Using pip3 (Install `PyQt5` and `Ekiga` separately using package manager )
```sh
pip3 install linuxdialer
sudo apt install python3-pyqt5 ekiga
```
Make sure you have `~/.local/bin` in `PATH` environment variable while installing using pip.

#### Run Application from command line

    linuxdialer -t tel:+919988776655
    linuxdialer -v path/to/vcardfile  

#### Test mime handler -

- Create a `test.html` containing `<a href="tel:+919988776655">Phone</a>`, open with firefox/chrome and click on Phone
- Click on a tel number in evolution addressbook
- Right click on a vCard file and open with Linux Dialer

##### Adding More buttons
One can add more buttons by looking at Button Creation Framework [Documentation](https://salsa.debian.org/sanjaypra555-guest/linux-dialer/blob/master/docs/Button%20Framework%20Documentation.md).

#### tel: link- 

![tel link demo](https://salsa.debian.org/uploads/-/system/temp/3d08295b790ddddbd6738aead9f33aae/tel.mp4)

#### vcard file
![vCard link demo](https://salsa.debian.org/uploads/-/system/temp/73b94de468dafdd779fefbbdd65f1f8f/vcard.mp4)